package finalTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class api_testing {


    @Test
    public void EnterNewEmployee() {

        Response response = RestAssured.given()
                .accept(ContentType.JSON)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"id\": \"3331\",\n" +
                        "    \"firstName\": \"Madi\",\n" +
                        "    \"lastName\": \"Makhmutov\",\n" +
                        "    \"role\": \"QA\",\n" +
                        "    \"department\": \"Security\"\n" +
                        "}")
                .post("https://employee-management-39848.herokuapp.com/api/employees")
                .then().extract().response();

        Assert.assertEquals("3331", response.jsonPath().getString("id"));
        Assert.assertEquals("Madi", response.jsonPath().getString("firstName"));
        Assert.assertEquals("Makhmutov", response.jsonPath().getString("lastName"));
        Assert.assertEquals("QA", response.jsonPath().getString("role"));
        Assert.assertEquals("Security", response.jsonPath().getString("department"));


    }
}
