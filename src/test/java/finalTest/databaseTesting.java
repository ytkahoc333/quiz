package finalTest;

import Utils.DBUtility;
import Utils.DBType;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

public class databaseTesting {
    @BeforeMethod
    public void setUp() {
        DBUtility.establishConnection(DBType.POSTGRESQL);
    }

    @AfterMethod
    public void closeDB() {
        DBUtility.closeConnections();
    }

    @Test
    public void test() {
        List<Map<String, Object>> empl = DBUtility.getQueryResults("Select * from employees LIMIT 10");
        for (int i = 0; i < empl.size(); i++) {
//            System.out.println(empl.get(i));
            Assert.assertEquals(empl.get(0).get("emp_no"), 10001);
            Assert.assertEquals(empl.get(0).get("first_name"), "Georgi");
            Assert.assertEquals(empl.get(0).get("last_name"), "Facello");

            Assert.assertEquals(empl.get(1).get("emp_no"), 10002);
            Assert.assertEquals(empl.get(1).get("first_name"), "Bezalel");
            Assert.assertEquals(empl.get(1).get("last_name"), "Simmel");

            Assert.assertEquals(empl.get(2).get("emp_no"), 10003);
            Assert.assertEquals(empl.get(2).get("first_name"), "Parto");
            Assert.assertEquals(empl.get(2).get("last_name"), "Bamford");

            Assert.assertEquals(empl.get(3).get("emp_no"), 10004);
            Assert.assertEquals(empl.get(3).get("first_name"), "Chirstian");
            Assert.assertEquals(empl.get(3).get("last_name"), "Koblick");

            Assert.assertEquals(empl.get(4).get("emp_no"), 10005);
            Assert.assertEquals(empl.get(4).get("first_name"), "Kyoichi");
            Assert.assertEquals(empl.get(4).get("last_name"), "Maliniak");

        }
    }
}
