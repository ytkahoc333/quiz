package Utils;

import Utils.ConfigurationReader;

import Utils.DBType;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtility {

    private static Connection connection;
    private static Statement statement;
    private static ResultSet resultSet;


    public static void establishConnection(DBType dbType) {
        try {
            switch (dbType) {
                case MYSQL:
                    break;
                case POSTGRESQL:
                    connection = DriverManager.getConnection(ConfigurationReader.getProperty("demoHR"),
                            ConfigurationReader.getProperty("dbUserName"),
                            ConfigurationReader.getProperty("dbPassword"));
                    if (connection != null) {
                        System.out.println("PostgreSQL DB connection Successfull!");
                    } else {
                        System.out.println("Failed to connect to PostgreSQL Database!");
                    }
                    break;
                case MONGDB:
                    break;
                case MARIADB:
                    break;
                case ORACLE:
                    break;
                default:
                    connection = null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void closeConnections() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
            System.out.println("DB closed successfully!");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Map<String, Object>> getQueryResults(String query) {
        try {
            statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            resultSet = statement.executeQuery(query);

            List<Map<String, Object>> list = new ArrayList<>();

            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

            int columnCount = resultSetMetaData.getColumnCount();

            while (resultSet.next()) {
                Map<String, Object> rowMap = new HashMap<>();
                // iterate column by column until the end
                for (int col = 1; col <= columnCount; col++) {
                    rowMap.put(resultSetMetaData.getColumnName(col), resultSet.getObject(col));
                }
                list.add(rowMap);
            }
            return list;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}

