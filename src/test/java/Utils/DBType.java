package Utils;

public enum DBType {
    POSTGRESQL, ORACLE, MYSQL, MONGDB, MARIADB
}
